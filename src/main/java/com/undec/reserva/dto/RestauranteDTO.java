package com.undec.reserva.dto;

import com.undec.reserva.model.Restaurante;

import java.util.ArrayList;
import java.util.List;

public class RestauranteDTO {
    private Integer id;
    private String nombre;
    private String direccion;
    private String descripcion;
    private String imagen;
    private String precio;
    private String categoria;
    private String url;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<RestauranteDTO> getRestauranteDTOList(List<Restaurante> restauranteList) {
        List<RestauranteDTO> restauranteDTOList = new ArrayList<>();
        for (Restaurante item : restauranteList) {
            RestauranteDTO restauranteDTO = new RestauranteDTO();
            restauranteDTO.setId(item.getId());
            restauranteDTO.setNombre(item.getNombre());
            restauranteDTO.setDireccion(item.getDireccion());
            restauranteDTO.setDescripcion(item.getDescripcion());
            restauranteDTO.setImagen(item.getImagen());
            restauranteDTO.setPrecio(item.getPrecio());
            restauranteDTO.setCategoria(item.getCategoria());
            restauranteDTO.setUrl(item.getUrl());
            restauranteDTOList.add(restauranteDTO);
        }
        return restauranteDTOList;
    }

    public RestauranteDTO getRestauranteDTO(Restaurante item) {
        RestauranteDTO restauranteDTO = new RestauranteDTO();
        restauranteDTO.setId(item.getId());
        restauranteDTO.setNombre(item.getNombre());
        restauranteDTO.setDireccion(item.getDireccion());
        return restauranteDTO;
    }
}
